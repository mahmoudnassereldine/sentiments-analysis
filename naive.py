
import nltk.classify.util
from nltk.classify import NaiveBayesClassifier
from nltk.corpus import names
from ntlk.corpus import stopwords
 

print stopwords.words('english')

def word_feats(words):
    return dict([(word, True) for word in words.split()])


positive_vocab = [ 'awesome', 'outstanding', 'fantastic', 'terrific', 'good', 'nice', 'great', ':)' ]
negative_vocab = [ 'bad', 'terrible','useless', 'hate', ':(' ]
neutral_vocab = [ 'movie','the','sound','was','is','actors','did','know','words','not' ]

positive_features = [(word_feats(pos), 'pos') for pos in positive_vocab]
negative_features = [(word_feats(neg), 'neg') for neg in negative_vocab]
neutral_features = [(word_feats(neu), 'neu') for neu in neutral_vocab]

# print(positive_features)
# print(negative_features)
# print(neutral_features)


train_set = negative_features + positive_features + neutral_features
print(train_set)
classifier = NaiveBayesClassifier.train(train_set) 

neg =0 
pos = 0

sentence = "Movies is good , Sound quality is bad"
sentence = sentence.lower()
words = sentence.split(' ')
for word in words:
    classResult = classifier.classify(word_feats(word))
    if(classResult =='pos'):
        pos=pos+1

    if(classResult =='neg'):
        neg=neg+1

print('Positive:'+str(float(pos)/len(words)))
print('Negative:'+str(float(neg)/len(words)))


