import pandas as pd
from bs4 import BeautifulSoup
import re
import nltk
from nltk.corpus import stopwords
from sklearn.feature_extraction.text import CountVectorizer
import numpy as np
from sklearn.ensemble import RandomForestClassifier

import matplotlib.pyplot as plt
from sklearn import model_selection
from sklearn.metrics import classification_report
from sklearn.metrics import confusion_matrix
from sklearn.metrics import accuracy_score
from sklearn.linear_model import LogisticRegression
from sklearn.tree import DecisionTreeClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.naive_bayes import GaussianNB
from sklearn.svm import SVC

nltk.download('stopwords')
data_training_path = "data/review/labeledTrainData.tsv"
data_unlabeled_path = "data/review/testData.tsv"
nbRow = 300
print("starting loading data ...")

def review_to_words(review):
        review_without_html = BeautifulSoup(review)
        letters_only = re.sub('[^a-zA-Z]', ' ', review_without_html.get_text())
        lower_case = letters_only.lower()
        words = lower_case.split()
        stops = set(stopwords.words(
            "english"))  
        meaningful_words = [w for w in words if not w in stops]
        return (" ".join(meaningful_words))


def convert_reviews_to_integer_array(train):  
    review_to_words(train['review'][0])
    num_reviews = train['review'].size
    # print(num_reviews)
    clean_train_reviews = []
    for i in range(0, nbRow):
        if (i % 1000 == 0):
            print(i)
        clean_train_reviews.append(review_to_words(train['review'][i]))

    print("finish")
    vectorizer = CountVectorizer(analyzer="word", tokenizer=None,preprocessor=None,stop_words=None,max_features=5000)
    train_data_features = vectorizer.fit_transform(clean_train_reviews)
    print("true")
    train_data_features = train_data_features.toarray()
    return train_data_features
 
   

def get_trained_model(train):
    train_data_features = convert_reviews_to_integer_array(train)
    forest = RandomForestClassifier(n_estimators = 100)
    
    forest = forest.fit(train_data_features,train['sentiment'])
    return forest

#read traning data 
train = pd.read_csv(data_training_path, header=0, delimiter='\t', quoting=3)
train_data_features = convert_reviews_to_integer_array(train)
print(train_data_features.shape)
print(train['sentiment'].shape)


models = []
models.append(('LR', LogisticRegression()))
models.append(('LDA', LinearDiscriminantAnalysis()))
models.append(('KNN', KNeighborsClassifier()))
models.append(('CART', DecisionTreeClassifier()))
models.append(('NB', GaussianNB()))
models.append(('SVM', SVC()))
# evaluate each model in turn
# Test options and evaluation metric
seed = 7
scoring = 'accuracy'
results = []
names = []
a = train['sentiment']
for name, model in models:
    kfold = model_selection.KFold(n_splits=10, random_state=seed)
    cv_results = model_selection.cross_val_score(
        model, train_data_features, a[:nbRow], cv=kfold, scoring=scoring)
    results.append(cv_results)
    names.append(name)
    msg = "%s: %f (%f)" % (name, cv_results.mean(), cv_results.std())
    print(msg)



# Compare Algorithms
fig = plt.figure()
fig.suptitle('Comparaison des Algorithmes')
ax = fig.add_subplot(111)
plt.boxplot(results)
print(results)
ax.set_xticklabels(names)
plt.show()
# forest = get_trained_model(train)

#read unlabeled data 
# unlabeled_data = pd.read_csv(data_training_path, header=0,delimiter="\t",quoting=3)
# unlabeled_data_vector = convert_reviews_to_integer_array(unlabeled_data)
# result = forest.predict(unlabeled_data_vector)
# output = pd.DataFrame( data={"id":unlabeled_data["id"], "sentiment":result} )
# output.to_csv( "Bag_of_Words_model.csv", index=False, quoting=3 )

