
import java.util.Collections;
import java.util.LinkedList;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilderFactory;



import javax.xml.parsers.DocumentBuilder;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.w3c.dom.Element;
import java.io.File;

public class Ville1 {

    public static void main(String[] args) {
        // Prints "Hello, World" to the terminal window.
        System.out.println("Hello, World");


            for(int x = 1; x < 100; x = x + 1) {
                if((x % 3) == 0) {
                    System.out.print("fizz : ");
                }
                if((x % 5) ==0) {
                    System.out.print("buzz :");
                }
                
                if( ((x % 5) ==0 ) && ((x % 3) ==0 ) )
               {
                System.out.print("FizzBuzz");
               }
               else {
                System.out.print(x);
               }

                System.out.print("\n");
             }

    }

    static public List<String> getVilleBydepartement(int dep ) {
        List<String> list = new ArrayList<String>();
        try {

            File fXmlFile = new File("villes_france.xml");
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(fXmlFile);
            doc.getDocumentElement().normalize();
            System.out.println("Root element :" + doc.getDocumentElement().getNodeName());
            NodeList nList = doc.getElementsByTagName("ville");
            System.out.println("----------------------------");

          
            for (int temp = 0; temp < nList.getLength(); temp++) {

                Node nNode = nList.item(temp);

                System.out.println("\nCurrent Element :" + nNode.getNodeName());

                if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                    Element eElement = (Element) nNode;
                    String dep1 = eElement.getElementsByTagName("ville_departement").item(0).getTextContent();
                    String ville = eElement.getElementsByTagName("nickname").item(0).getTextContent();
                     
                    if(Integer.toString(dep).equals(dep1.toString())){
                        list.add(ville);
                    }

                    System.out.println("Staff id : " + eElement.getAttribute("vid"));
                    // System.out.println("----------------------------");
                    System.out.println("ville_departement : "
                            + eElement.getElementsByTagName("ville_departement").item(0).getTextContent());
                    // System.out.println("ville_nom_simple: "
                    //         + eElement.getElementsByTagName("ville_nom_simple").item(0).getTextContent());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return list;

    }

    static public String getVilleBycodepostal(int codepostal) {
        String list = "";
        try {

            File fXmlFile = new File("villes_france.xml");
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(fXmlFile);
            doc.getDocumentElement().normalize();
            System.out.println("Root element :" + doc.getDocumentElement().getNodeName());
            NodeList nList = doc.getElementsByTagName("ville");
            System.out.println("----------------------------");

          
            for (int temp = 0; temp < nList.getLength(); temp++) {

                Node nNode = nList.item(temp);

                System.out.println("\nCurrent Element :" + nNode.getNodeName());

                if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                    Element eElement = (Element) nNode;
                    String codepostal1 = eElement.getElementsByTagName("code_postal").item(0).getTextContent();
                    String ville = eElement.getElementsByTagName("nickname").item(0).getTextContent();
                     
                    if(Integer.toString(codepostal).equals(codepostal1.toString())){
                        list = ville ;
                    }

                    System.out.println("Staff id : " + eElement.getAttribute("vid"));
                    // System.out.println("----------------------------");
                    System.out.println("ville_departement : "
                            + eElement.getElementsByTagName("ville_departement").item(0).getTextContent());
                    // System.out.println("ville_nom_simple: "
                    //         + eElement.getElementsByTagName("ville_nom_simple").item(0).getTextContent());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return list;

    }





}